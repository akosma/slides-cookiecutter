= {{cookiecutter.title}}

These are the slides of the presentation name "{{cookiecutter.title}}."

== Requirements

You must have the latest version of https://podman.io/[Podman] installed locally.

To run the `make chromium` task you need Chromium installed via Flatpak.

To generate PowerPoint and ODP files you need https://www.libreoffice.org/[LibreOffice].

== Build

Check the https://docs.asciidoctor.org/reveal.js-converter/latest/[Asciidoctor Reveal.js documentation] to learn how to customize the `slides.adoc` file.

* Use the `make` command to build the `slides.html` file.
* Other outputs: `make slides.pdf`, `make slides.odp`, and `make slides.pptx`
* Use `make clean` to get rid of all build artifacts.

The file `slides.html` is a https://revealjs.com/[Reveal.js presentation].
