:revealjsdir: resources/reveal.js
:revealjs_autoPlayMedia: false
:revealjs_backgroundTransition: none
:revealjs_controls: false
:revealjs_controlsTutorial: false
:revealjs_history: true
:revealjs_navigationMode: linear
:revealjs_pdfSeparateFragments: false
:revealjs_preloadIframes: true
:revealjs_showSlideNumber: speaker
:revealjs_slideNumber: true
:revealjs_theme: {{cookiecutter.theme}}
:revealjs_totalTime: 1800
:revealjs_transition: {{cookiecutter.transition}}
:revealjs_width: 1280
:revealjs_height: 720

:source-highlighter: highlight.js
:highlightjs-theme: resources/styles/{{cookiecutter.highlightjs_theme}}.min.css

:doctitle: {{cookiecutter.title}}
:author: {{cookiecutter.author_name}}
:email: {{cookiecutter.author_email}}
:revdate: {docdate}
:icons: font
:experimental:
:imagesdir: assets/images
:hide-uri-scheme:


[.notes]
--
Speaker notes here.
--



== Bullet Points

* Foo
* Bar
* World

[.notes]
--
Speaker notes here.
--



== Code

[source,c]
--
#include<stdio.h>

int main(void) {
    printf("Hello World\n"); // <1>
    return 0;
}
--
<1> You know the drill

[.notes]
--
Speaker notes here.
--



== Table

[cols=3*,options=header]
|===
|Name |Group |Version

|Firefox
|Web Browser
|45

|Ruby
|Programming Language
|2.8

|Podman
|DevOps Tool
|4.0

|===



== Admonition

[NOTE]
====
This is an example of an admonition block.

* NOTE
* TIP
* WARNING
* CAUTION
* IMPORTANT
====



== Quote

.Gettysburg Address
[quote, Abraham Lincoln, Address delivered at the dedication of the Cemetery at Gettysburg]
____
Four score and seven years ago our fathers brought forth on this continent a new nation...
____



== Icons

icon:heart[2x,role=red]



== Buttons and Menus

[%step]
. Press the btn:[OK] button when you are finished.
. To save the file, select menu:File[Save].
. Select menu:View[Zoom > Reset] to reset the zoom level to the default setting.
. Press kbd:[Ctrl + F11] when ready.



== Footnotes

The hail-and-rainbow protocolfootnote:[The double hail-and-rainbow level makes my toes tingle.] can be initiated at five levels. A bold statement!footnote:disclaimer[Opinions are my own.]



[%notitle]
== Images

image::random.jpg[width=500]

[.small]
Photo by https://unsplash.com/@ninjason?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText[Jason Leung] on https://unsplash.com/photos/Xaanw0s0pMk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText[Unsplash]

